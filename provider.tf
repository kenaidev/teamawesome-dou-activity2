#provider.tf
provider "azurerm" {
  features {}
}

#terraform.tf
terraform {
  backend "azurerm" {
    resource_group_name  = "TeamAwesome"
    storage_account_name = "tower02412"
    container_name       = "thecontainer"
    key                  = "terraform.tfstate"
  }
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.0"
    }
  }
}


