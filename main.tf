data "azurerm_resource_group" "my-rg" {
  name = "TeamAwesome"
}


resource "azurerm_virtual_network" "network" {
  name                = "elQuequiera"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.my-rg.location
  resource_group_name = data.azurerm_resource_group.my-rg.name
}

## add your subnet module code here

resource "azurerm_subnet" "example" {
  name                 = "moon-subnet"
  resource_group_name  = "TeamAwesome"
  virtual_network_name = "elQuequiera"
  address_prefix    =  "10.0.1.0/24"

  delegation {
    name = "delegation"

    service_delegation {
      name    = "Microsoft.ContainerInstance/containerGroups"
      actions = ["Microsoft.Network/virtualNetworks/subnets/join/action", "Microsoft.Network/virtualNetworks/subnets/prepareNetworkPolicies/action"]
    }
  }
}
